//
//  DreamsTableViewCell.swift
//  DreamSnatcher
//
//  Created by pwnstarSmithy on 22/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DreamsTableViewCell: UITableViewCell {

    @IBOutlet weak var dreamTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
