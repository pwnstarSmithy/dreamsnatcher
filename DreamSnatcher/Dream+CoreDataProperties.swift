//
//  Dream+CoreDataProperties.swift
//  DreamSnatcher
//
//  Created by pwnstarSmithy on 22/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//
//

import Foundation
import CoreData


extension Dream {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Dream> {
        return NSFetchRequest<Dream>(entityName: "Dream")
    }

    @NSManaged public var title: String?
    @NSManaged public var dreamDescription: String?
    @NSManaged public var tag: String?

}
