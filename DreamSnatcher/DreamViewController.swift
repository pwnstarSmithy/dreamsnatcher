//
//  DreamViewController.swift
//  DreamSnatcher
//
//  Created by pwnstarSmithy on 22/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DreamViewController: UIViewController {

    @IBOutlet weak var dreamTitle: UILabel!
    
    
    @IBOutlet weak var dreamDescription: UITextView!
    
    @IBOutlet weak var tagOne: UILabel!
    
    @IBOutlet weak var tagTwo: UILabel!
    
    @IBOutlet weak var tagThree: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dreamTitle.text = selectedDreamTitle
        dreamDescription.text = selectedDreamDescription
        tagTwo.text = selectedDreamTag
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
