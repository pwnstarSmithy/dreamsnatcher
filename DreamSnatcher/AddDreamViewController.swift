//
//  AddDreamViewController.swift
//  DreamSnatcher
//
//  Created by pwnstarSmithy on 22/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class AddDreamViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet weak var addTitle: UITextField!
    
    @IBOutlet weak var addDescription: UITextView!
    
    @IBOutlet weak var addTags: UITextField!
    
    @IBAction func submitDraem(_ sender: Any) {
        
        if addTitle.text != nil && addDescription != nil {
            
            let dreamTitle = addTitle.text
            let dreamDescription = addDescription.text
            let dreamTag = addTags.text
            
            let dream = Dream(context: PersistentService.context)
            
            dream.title = dreamTitle 
            dream.dreamDescription = dreamDescription
            dream.tag = dreamTag
            
            PersistentService.saveContext()
            
            dreams.append(dream)
            
        }else{
            
            print("Please enter title and description!")
            
        }
        
        self.navigationController?.popViewController(animated: true)
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

       self.addTitle.delegate = self
        self.addDescription.delegate = self
        self.addTags.delegate = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
